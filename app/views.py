from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app, db, lm, oid
from app.forms import LoginForm, ProfileForm
from app.models import User, ROLE_USER, ROLE_ADMIN

"""
Views holds all the webpage views. Add to this file to add new pages to the
application.
"""

#Load a user form the database
@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.before_request
def before_request():
    g.user = current_user

@oid.after_login
def after_login(resp):
    if resp.email is None or resp.email == "":
        flash('Invalid login. Please try again')
        return redirect(url_for('login'))
    user = User.query.filter_by(email = resp.email).first()
    if user is None:
        return redirect(url_for('create_profile'))
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember = remember_me)
    return redirect(request.args.get('next') or url_for('index'))

#Index view is the homepage of the application
@app.route('/')
@app.route('/index')
def index():
    user = g.user
    return render_template("index.html", 
            title = 'Triangle',
            user = user)

#login handling functionality
@app.route('/login', methods = ['GET', 'POST'])
@oid.loginhandler
def login():
    if g.user is not None and g.user.is_authenticated():
        flash('Sucessfully signed in')
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        session['remember_me'] = form.remember_me.data
        return oid.try_login(form.openid.data, ask_for = ['nickname', 'email'])
    return render_template('login.html',
            title = 'Sign In',
            form = form,
            providers = app.config['OPENID_PROVIDERS'])

@app.route('/createprofile', methods=['GET', 'POST'])
def create_profile():
    form = ProfileForm()
    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        cell = form.cell.data
        if not name:
            flash('Error: You have to provide a name')
        elif '@' not in email:
            flash('Error: You have to enter a valid email address')
        else:
            flash('Profile sucessfully created')
            user = User(nickname = name, email = email, cell = cell, role = ROLE_USER)
            db.session.add(user)
            db.session.commit()
            login_user(user)
            return redirect(url_for('index'))
    return render_template('profile.html', title = 'Create Profile', form=form)
        
@app.route('/logout')
def logout():
    logout_user()
    flash('You were signed out')
    return redirect(url_for('index'))
