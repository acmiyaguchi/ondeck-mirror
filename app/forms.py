from flask.ext.wtf import Form
from wtforms import TextField, BooleanField
from wtforms.validators import Required, Email

class LoginForm(Form):
    openid = TextField('openid', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)

class ProfileForm(Form):
    name = TextField('username', validators = [Required()])
    email = TextField('email', validators = [Required(), Email()])
    cell = TextField('cell')
